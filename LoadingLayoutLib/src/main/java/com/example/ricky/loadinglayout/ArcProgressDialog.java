package com.example.ricky.loadinglayout;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;

/**
 * Created by Ricky on 2017/4/18 0018.
 */

public class ArcProgressDialog extends Dialog {

    public ArcProgressDialog(Context context){
        this(context, R.style.ProgressDialog);
    }

    private ArcProgressDialog(Context context,int theme){
        super(context, theme);
        this.setContentView(R.layout.layout_dialog_arc);
        this.getWindow().getAttributes().gravity = Gravity.CENTER;
    }
}
