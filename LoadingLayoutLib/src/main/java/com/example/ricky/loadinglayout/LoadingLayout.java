package com.example.ricky.loadinglayout;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Ricky on 2017/4/17 0017.
 */

public class LoadingLayout extends RelativeLayout implements View.OnClickListener {

    public static final int LOADING = 0;
    public static final int EMPTY_DATA = 1;
    public static final int ERROR = 2;
    public static final int NO_NETWORK = 3;
    public static final int CONTENT = 4;

    @IntDef({LOADING, EMPTY_DATA, ERROR, NO_NETWORK, CONTENT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ViewLayer {
    }

    @LoadingLayout.ViewLayer
    private int mDisplayViewLayer = LOADING;

    private LinearLayout mEmptyLayer, mErrorLayer, mNoNetworkLayer;

    private View mContentLayer;

    private ImageView mEmptyLayerImage, mErrorLayerImage, mNoNetworkLayerImage;

    private TextView mEmptyLayerDescription, mErrorLayerDescription, mNoNetworkDescription,
            mErrorLayerRepeat, mNoNetworkRepeat;

    private GifImageView mLoadingLayer;

    private LoadingLayoutConfig.IOnLoadingRepeat mOnLoadingRepeatListener;

    private LoadingLayoutConfig mConfig;

    private Context mContext;

    public LoadingLayout(@NonNull Context context) {
        super(context);
        setUp(context);
    }

    public LoadingLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setUp(context);
    }

    public LoadingLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUp(context);
    }

    private void setUp(Context context) {
        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.layout_loading, this, true);
        findView();
        readConfig();
    }

    private void findView() {
        mLoadingLayer = (GifImageView) findViewById(R.id.loading);
        mEmptyLayer = (LinearLayout) findViewById(R.id.empty_data);
        mErrorLayer = (LinearLayout) findViewById(R.id.error);
        mNoNetworkLayer = (LinearLayout) findViewById(R.id.no_network);
        mEmptyLayerImage = (ImageView) findViewById(R.id.empty_image);
        mErrorLayerImage = (ImageView) findViewById(R.id.error_image);
        mNoNetworkLayerImage = (ImageView) findViewById(R.id.no_network_image);
        mEmptyLayerDescription = (TextView) findViewById(R.id.empty_description);
        mErrorLayerDescription = (TextView) findViewById(R.id.error_description);
        mNoNetworkDescription = (TextView) findViewById(R.id.no_network_description);
        mErrorLayerRepeat = (TextView) findViewById(R.id.error_repeat);
        mErrorLayerRepeat.setOnClickListener(this);
        mNoNetworkRepeat = (TextView) findViewById(R.id.no_network_repeat);
        mNoNetworkRepeat.setOnClickListener(this);
    }

    private void readConfig() {
        mConfig = LoadingLayoutConfig.getInstance();
        mLoadingLayer.setImageResource(mConfig.getLoadingGifId());
        mEmptyLayerImage.setImageResource(mConfig.getEmptyImageResId());
        mErrorLayerImage.setImageResource(mConfig.getErrorImageResId());
        mNoNetworkLayerImage.setImageResource(mConfig.getNoNetworkImageResId());
        mEmptyLayerDescription.setText(mConfig.getEmptyDescription());
        mErrorLayerDescription.setText(mConfig.getErrorDescription());
        mNoNetworkDescription.setText(mConfig.getNoNetworkDescription());
        mErrorLayerRepeat.setText(mConfig.getErrorRepeat());
        mNoNetworkRepeat.setText(mConfig.getNoNetworkRepeat());
    }

    private void judgeDisplayLayer() {
        do {

            if (mDisplayViewLayer == LOADING) {
                mLoadingLayer.setVisibility(VISIBLE);
                mEmptyLayer.setVisibility(GONE);
                mErrorLayer.setVisibility(GONE);
                mNoNetworkLayer.setVisibility(GONE);
                mContentLayer.setVisibility(GONE);
                break;
            }

            if (mDisplayViewLayer == EMPTY_DATA) {
                mLoadingLayer.setVisibility(GONE);
                mEmptyLayer.setVisibility(VISIBLE);
                mErrorLayer.setVisibility(GONE);
                mNoNetworkLayer.setVisibility(GONE);
                mContentLayer.setVisibility(GONE);
                break;
            }

            if (mDisplayViewLayer == ERROR) {
                mLoadingLayer.setVisibility(GONE);
                mEmptyLayer.setVisibility(GONE);
                mErrorLayer.setVisibility(VISIBLE);
                mNoNetworkLayer.setVisibility(GONE);
                mContentLayer.setVisibility(GONE);
                break;
            }

            if (mDisplayViewLayer == NO_NETWORK) {
                mLoadingLayer.setVisibility(GONE);
                mEmptyLayer.setVisibility(GONE);
                mErrorLayer.setVisibility(GONE);
                mNoNetworkLayer.setVisibility(VISIBLE);
                mContentLayer.setVisibility(GONE);
                break;
            }

            if (mDisplayViewLayer == CONTENT) {
                mLoadingLayer.setVisibility(GONE);
                mEmptyLayer.setVisibility(GONE);
                mErrorLayer.setVisibility(GONE);
                mNoNetworkLayer.setVisibility(GONE);
                mContentLayer.setVisibility(VISIBLE);
                break;
            }

        } while (false);
    }

    @Override
    public void onClick(View view) {
        do {

            if (view.getId() == R.id.error_repeat && mOnLoadingRepeatListener != null) {
                mOnLoadingRepeatListener.onErrorRepeat();
                break;
            }

            if (view.getId() == R.id.no_network_repeat && mOnLoadingRepeatListener != null) {
                mOnLoadingRepeatListener.onNoNetworkRepeat();
                break;
            }

        } while (false);
    }

    public void prepare(){
        mContentLayer = getChildAt(getChildCount() - 1);
        judgeDisplayLayer();
    }

    public void setDisplayViewLayer(@ViewLayer int viewLayer) {
        mDisplayViewLayer = viewLayer;
        judgeDisplayLayer();
    }

    @LoadingLayout.ViewLayer
    public int getDisplayViewLayer() {
        return mDisplayViewLayer;
    }

    public LoadingLayoutConfig.IOnLoadingRepeat getOnLoadingRepeatListener() {
        return mOnLoadingRepeatListener;
    }

    public void setOnLoadingRepeatListener(LoadingLayoutConfig.IOnLoadingRepeat onLoadingRepeatListener) {
        mOnLoadingRepeatListener = onLoadingRepeatListener;
    }

}
