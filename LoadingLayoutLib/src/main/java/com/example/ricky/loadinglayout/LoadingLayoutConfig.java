package com.example.ricky.loadinglayout;

/**
 * Created by Ricky on 2017/4/17 0017.
 */

public class LoadingLayoutConfig {

    private LoadingLayoutConfig() {

    }

    private int mLoadingGifId;

    private int mEmptyImageResId;

    private String mEmptyDescription;

    private int mErrorImageResId;

    private String mErrorDescription;

    private String mErrorRepeat;

    private int mNoNetworkImageResId;

    private String mNoNetworkDescription;

    private String mNoNetworkRepeat;

    public int getEmptyImageResId() {
        return mEmptyImageResId;
    }

    public LoadingLayoutConfig setEmptyImageResId(int emptyImageResId) {
        mEmptyImageResId = emptyImageResId;
        return this;
    }

    public String getEmptyDescription() {
        return mEmptyDescription;
    }

    public LoadingLayoutConfig setEmptyDescription(String emptyDescription) {
        mEmptyDescription = emptyDescription;
        return this;
    }

    public int getErrorImageResId() {
        return mErrorImageResId;
    }

    public LoadingLayoutConfig setErrorImageResId(int errorImageResId) {
        mErrorImageResId = errorImageResId;
        return this;
    }

    public String getErrorDescription() {
        return mErrorDescription;
    }

    public LoadingLayoutConfig setErrorDescription(String errorDescription) {
        mErrorDescription = errorDescription;
        return this;
    }

    public String getErrorRepeat() {
        return mErrorRepeat;
    }

    public LoadingLayoutConfig setErrorRepeat(String errorRepeat) {
        mErrorRepeat = errorRepeat;
        return this;
    }

    public int getNoNetworkImageResId() {
        return mNoNetworkImageResId;
    }

    public LoadingLayoutConfig setNoNetworkImageResId(int noNetworkImageResId) {
        mNoNetworkImageResId = noNetworkImageResId;
        return this;
    }

    public String getNoNetworkDescription() {
        return mNoNetworkDescription;
    }

    public LoadingLayoutConfig setNoNetworkDescription(String noNetworkDescription) {
        mNoNetworkDescription = noNetworkDescription;
        return this;
    }

    public String getNoNetworkRepeat() {
        return mNoNetworkRepeat;
    }

    public LoadingLayoutConfig setNoNetworkRepeat(String noNetworkRepeat) {
        mNoNetworkRepeat = noNetworkRepeat;
        return this;
    }

    private static class ConfigHolder {
        private static final LoadingLayoutConfig INSTANCE = new LoadingLayoutConfig();
    }

    public static final LoadingLayoutConfig getInstance() {
        return ConfigHolder.INSTANCE;
    }


    public int getLoadingGifId() {
        return mLoadingGifId;
    }

    public LoadingLayoutConfig setLoadingGifId(int mLoadingGifId) {
        this.mLoadingGifId = mLoadingGifId;
        return this;
    }

    public interface IOnLoadingRepeat {
        void onErrorRepeat();

        void onNoNetworkRepeat();
    }

}
