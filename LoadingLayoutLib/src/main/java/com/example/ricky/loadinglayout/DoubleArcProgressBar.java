package com.example.ricky.loadinglayout;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;


/**
 * Created by Ricky on 2017/4/17 0017.
 */

public class DoubleArcProgressBar extends View {

    private int mOuterColor;

    private int mInnerColor;

    private int mArcWidth;

    private int mOuterArcSpeed;

    private int mInnerArcSpeed;

    private Paint mPaint;

    private RectF mOuterOval;

    private RectF mInnerOval;

    private int mOuterStartAngle;

    private int mOuterEndAngle = -300;

    private float mInnerStartAngle;

    private float mInnerEndAngle = 300f;

    private ObjectAnimator mOuterArcStartAnimator;

    private ObjectAnimator mInnerArcStartAnimator;

    private AnimatorSet mAnimatorSet;

    public DoubleArcProgressBar(Context context) {
        super(context);
    }

    public DoubleArcProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray attrArray = context.obtainStyledAttributes(attrs, R.styleable.DoubleArcProgressBar);

        int indexCount = attrArray.getIndexCount();

        for (int i = 0; i < indexCount; i++) {

            int index = attrArray.getIndex(i);

            do {
                if (index == R.styleable.DoubleArcProgressBar_outerArcColor) {
                    mOuterColor = attrArray.getColor(i, Color.parseColor("#336699"));
                    break;
                }

                if (index == R.styleable.DoubleArcProgressBar_innerArcColor) {
                    mInnerColor = attrArray.getColor(i, Color.parseColor("#33cc99"));
                    break;
                }

                if (index == R.styleable.DoubleArcProgressBar_arcWidth) {
                    mArcWidth = attrArray.getDimensionPixelOffset(i, (int) TypedValue.
                            applyDimension(TypedValue.COMPLEX_UNIT_PX, 6,
                                    getResources().getDisplayMetrics()));
                    break;
                }

                if (index == R.styleable.DoubleArcProgressBar_outerArcSpeed) {
                    mOuterArcSpeed = attrArray.getInt(i, 800);
                    break;
                }

                if (index == R.styleable.DoubleArcProgressBar_innerArcSpeed) {
                    mInnerArcSpeed = attrArray.getInt(i, 500);
                    break;
                }
            } while (false);
        }

        attrArray.recycle();

        mPaint = new Paint();

        mOuterOval = new RectF();
        mInnerOval = new RectF();

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(mArcWidth);
        mPaint.setStyle(Paint.Style.STROKE);

        mPaint.setColor(mOuterColor);
        canvas.drawArc(mOuterOval, mOuterStartAngle, mOuterEndAngle, false, mPaint);

        mPaint.setColor(mInnerColor);
        canvas.drawArc(mInnerOval, mInnerStartAngle, mInnerEndAngle, false, mPaint);
    }

    @Override
    public void layout(int l, int t, int r, int b) {
        super.layout(l, t, r, b);
        initOvalAttrs();
    }

    private void initOvalAttrs() {

        int mArcGap;

        int halfWidth = mArcWidth / 2;

        Point centerPoint = new Point();
        centerPoint.x = getWidth() / 2;
        centerPoint.y = getHeight() / 2;

        mOuterOval.left = getPaddingLeft() + halfWidth;
        mOuterOval.top = getPaddingTop() + halfWidth;
        mOuterOval.right = getWidth() - getPaddingRight() - halfWidth;
        mOuterOval.bottom = getHeight() - getPaddingBottom() - halfWidth;

        float mOuterRadius = mOuterOval.width() / 2 - halfWidth;

        mArcGap = (int) (mOuterRadius / 4);

        float mInnerRadius = mOuterRadius - mArcWidth - mArcGap;

        mInnerOval.left = centerPoint.x - mInnerRadius;
        mInnerOval.top = centerPoint.y - mInnerRadius;
        mInnerOval.right = centerPoint.x + mInnerRadius;
        mInnerOval.bottom = centerPoint.y + mInnerRadius;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        createAnimator();
        mAnimatorSet.playTogether(mOuterArcStartAnimator, mInnerArcStartAnimator);
        mAnimatorSet.start();
    }

    private void createAnimator() {

        mOuterStartAngle = -30;

        mInnerStartAngle = -90f;

        mOuterArcStartAnimator = ObjectAnimator.ofInt(this, "mOuterStartAngle", mOuterStartAngle, 330);
        mOuterArcStartAnimator.setDuration(mOuterArcSpeed);

        mInnerArcStartAnimator = ObjectAnimator.ofFloat(this, "mInnerStartAngle", mInnerStartAngle, -450);
        mInnerArcStartAnimator.setDuration(mInnerArcSpeed);

        setAnimatorsMode(mOuterArcStartAnimator, mInnerArcStartAnimator);

        mOuterArcStartAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mOuterStartAngle = (int) animation.getAnimatedValue();
                invalidate();
            }
        });
        mInnerArcStartAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mInnerStartAngle = (float) animation.getAnimatedValue();
                invalidate();
            }
        });

        mAnimatorSet = new AnimatorSet();

    }

    private void setAnimatorsMode(ValueAnimator... animators) {
        for (ValueAnimator animator : animators) {
            animator.setInterpolator(new LinearInterpolator());
            animator.setRepeatCount(ValueAnimator.INFINITE);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mAnimatorSet.cancel();
    }

    public void setMOuterStartAngle(int mOuterStartAngle) {
        this.mOuterStartAngle = mOuterStartAngle;
    }

    public void setMInnerStartAngle(float mInnerStartAngle) {
        this.mInnerStartAngle = mInnerStartAngle;
    }

    public void setArcWidth(int arcWidth) {
        mArcWidth = arcWidth;
    }

    public void setOuterColor(String outerColor) {
        mOuterColor = Color.parseColor(outerColor);
    }

    public void setInnerColor(String innerColor) {
        mInnerColor = Color.parseColor(innerColor);
    }
}
